#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

#r @"System.Xml.Linq"

module Util =
  
  module Xml =
    
    open System.Xml
    open System.Xml.Linq
    
    let private isNull : 'a -> bool =
      fun x ->
        x = Unchecked.defaultof<_>
        
    let xname : string -> XName =
      fun str ->
        XName.Get str
    
    let parse : string -> XDocument = XDocument.Parse

module DonSyme =
  
  (* Don Syme Blog: "Introducing F# Asynchronous Workflows"
     
     https://blogs.msdn.microsoft.com/dsyme/
     2007/10/10/introducing-f-asynchronous-workflows/ *)
  
  open System.IO
  open System.Net
  
  let syncHttp : string -> string =
    fun url ->
      // Create the web request object
      let req = WebRequest.Create url
      
      // Get the response, synchronously
      let rsp = req.GetResponse ()
      
      // Grab the response stream and a reader. Clean up when we're done
      use stream = rsp.GetResponseStream ()
      use reader = new StreamReader(stream)
      
      // Synchronous read-to-end, returning the result
      reader.ReadToEnd() 
  
  let asyncHttp : string -> string Async =
    fun url ->
      async {
        // Create the web request object
        let req = WebRequest.Create url
        
        // Get the response, asynchronously
        // let! rsp = req.GetResponseAsync () (* API changes since blog post *)
        let! rsp = req.AsyncGetResponse ()
        
        // Grab the response stream and a reader. Clean up when we're done
        use stream = rsp.GetResponseStream ()
        use reader = new StreamReader(stream)
        
        // synchronous read-to-end
        return reader.ReadToEnd ()
      }

let hasHelloWorld : string -> string Async -> (string * bool) Async =
  fun name ahtml ->
    async {
      let! html  = ahtml
      let  hashw =
        html.Contains("Hello World!")  ||
        html.Contains("Hello world!")  ||
        html.Contains("Hello, World!") ||
        html.Contains("Hello, world!")
      
      return (name,hashw)
    }

let wiki = @"http://en.wikipedia.org"
let list = @"/wiki/List_of_programming_languages"

let html =
  wiki + list
  |> DonSyme.syncHttp
  |> Util.Xml.parse

"a" (* Find all <a href ... /> in the html page *)
|> Util.Xml.xname
|> html.Document.Descendants
|> Seq.map    (fun    x  -> x.Value,wiki + x.FirstAttribute.Value)
|> Seq.filter (fun (_,y) -> y.Contains "/wiki/")
|> Seq.toArray
|> Array.Parallel.map (
  fun (name,url) ->
    (* TODO *)
)
