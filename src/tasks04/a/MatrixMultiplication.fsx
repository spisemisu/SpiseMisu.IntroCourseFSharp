#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

type 'a Matrix = 'a [][]

let inline add (x:^a Matrix) (y:^a Matrix) : ^a Matrix when
  (^a) : (static member ( + ) : ^a * ^a -> ^a) =

  (* TODO *)

let inline mul (x:^a Matrix) (y:^a Matrix) : ^a Matrix when
  (^a) : (static member ( + ) : ^a * ^a -> ^a) and
  (^a) : (static member ( * ) : ^a * ^a -> ^a) =

  (* TODO *)
