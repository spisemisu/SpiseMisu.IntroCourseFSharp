#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

type 'a Matrix = 'a [][]

let inline add (x:^a Matrix) (y:^a Matrix) : ^a Matrix when
  (^a) : (static member ( + ) : ^a * ^a -> ^a) =
  let (xi,xj) =
    if Array.isEmpty x then
      (0,0)
    else
      Array.length x, Array.length x.[0]
  let (yi,yj) =
    if Array.isEmpty y then
      (0,0)
    else
      Array.length y, Array.length y.[0]
    
  if xi = yi && xj = yj then
    (* https://en.wikipedia.org/wiki/Matrix_addition *)
    Array.Parallel.init xi (
      fun i ->
        Array.Parallel.init xj (
          fun j ->
            x.[i].[j] + y.[i].[j]
        )
    )
  else
    failwith "Matrices must have equal number of rows & columns to be added"

let inline mul (x:^a Matrix) (y:^a Matrix) : ^a Matrix when
  (^a) : (static member ( + ) : ^a * ^a -> ^a) and
  (^a) : (static member ( * ) : ^a * ^a -> ^a) =
  let (xi,xj) =
    if Array.isEmpty x then
      (0,0)
    else
      Array.length x, Array.length x.[0]
  let (yi,yj) =
    if Array.isEmpty y then
      (0,0)
    else
      Array.length y, Array.length y.[0]
  
  (* https://en.wikipedia.org/wiki/Matrix_multiplication *)
  Array.Parallel.init xi (
    fun i ->
      Array.Parallel.init yj (
        fun j ->
          Array.Parallel.init xj (fun k -> x.[i].[k] * y.[k].[j])
          |> Array.sum (* |> Array.reduce (+) *)
      )
  )
