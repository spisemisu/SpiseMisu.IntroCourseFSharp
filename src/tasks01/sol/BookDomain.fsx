#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"
  
module Format =
    
  type audio      = AAC  | MP3  | M4B | WAV
  type electronic = EPUB | MOBI | PDF
  type physical   = Hardcover   | Paperback
  
type author   = Author of name:string * surname:string
type language = English | Danish | Spanish
type isbn10   = ISBN10 (* TODO in tasks02 *)
type isbn13   = ISBN13 (* TODO in tasks02 *)
  
type common =
  { title:      string
    authors:    author list 
    publisher:  string
    language:   language
    isbn10:     isbn10     
    isbn13:     isbn13
  }
and book =
  | Audio      of audio
  | Electronic of electronic
  | Physical   of physical
and audio =
  { common: common
    format: Format.audio
  }
and electronic =
  { common: common
    pages:  uint32
    format: Format.electronic
  }
and physical =
  { common: common
    pages:  uint32
    format: Format.physical
  }
