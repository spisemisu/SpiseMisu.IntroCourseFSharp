#!/usr/bin/env fsharpi

let infinite : bigint seq =
  let rec hlp n =
    seq { yield  n
          yield! hlp (n + 1I) }
  
  hlp 0I
