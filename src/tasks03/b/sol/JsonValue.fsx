#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

(* warning FS0060: Override implementations in augmentations are now
deprecated. Override implementations should be given as part of the initial
declaration of a type. *)
#nowarn "60"


module Json =
  
  (* http://json.org/ *)
  type value =
    | String  of string
    | Number  of float
    | Object  of (string,value) Map
    | Array   of value list
    | Boolean of bool
    | Null
  
  let rec private json2str : value -> string = function
    | String  s  -> s  |> sprintf "\"%s\""
    | Number  n  -> n  |> sprintf "%f"
    | Object  xs -> xs |> objHlp
    | Array   xs -> xs |> arrHlp
    | Boolean b  -> b  |> sprintf "%b"
    | Null       -> "null"
  
  and private objHlp : (string,value) Map -> string =
    fun obj ->
      match obj |> Map.isEmpty with
        | true  -> "{ }"
        | false ->
          obj
          |> Map.fold (
            fun acc key value ->
              let k = key   |> String |> json2str
              let v = value           |> json2str
              (sprintf "%s: %s" k v) :: acc
          ) []
          |> List.reduce (fun x y -> sprintf "%s, %s" x y)
          |> sprintf "{ %s }"
  
  and private arrHlp : value list -> string = function
    | [] -> "[ ]"
    | xs ->
      xs
      |> List.map json2str
      |> List.reduce (fun x y -> sprintf "%s, %s" x y)
      |> sprintf "[ %s ]"
  
  type value with override json.ToString() = json |> json2str
