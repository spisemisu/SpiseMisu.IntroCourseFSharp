#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

let inline (>>=) m f = Option.bind f m

module Random =
  let private r        = new System.Random()
  let next lower upper = r.Next(lower,upper)

module Movie =
  
  (* Sometimes, size matters *)
  sizeof<byte>  (* > val it : int = 1 *)
  sizeof<int16> (* > val it : int = 2 *)
  sizeof<int>   (* > val it : int = 4 *)
  
  type rating = private Rating of byte with
    override rate.ToString() =
      let (Rating r) = rate
      r |> sprintf "%i"
  
  let value : rating -> int =
    fun (Rating b) ->
      b |> int
    
  let rate : int -> rating option =
    fun n ->
      if n <= 5 && n >= 0 then
        n |> byte |> Rating |> Some
      else
        None

//List.init 25 (fun _ -> Random.next -10 10 |> Some >>= Movie.rate)
List.init 25 (fun _ -> Random.next -10 10 |> Movie.rate)
|> List.choose id (* Filter away all None values (fake reviews) *)
//|> List.choose (fun x -> printfn "%A" x; x) (* Debug version *)
|> List.averageBy (Movie.value >> float)
