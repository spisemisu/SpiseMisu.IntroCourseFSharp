#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

let inline (>>=) m f = Option.bind f m

module Random =
  let private r        = new System.Random()
  let next lower upper = r.Next(lower,upper)

module Movie =
  
  (* TODO *)
