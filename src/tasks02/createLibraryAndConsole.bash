#!/bin/bash

clear

# create library app with .NET Core 2.x
dotnet new classlib -lang F# -o Library
cd Library
dotnet restore
dotnet build -c Release
cd ../

# create console app with .NET Core 2.x
dotnet new console -lang F# -o Console
cd Console
dotnet restore
dotnet build -c Release
cd ../
