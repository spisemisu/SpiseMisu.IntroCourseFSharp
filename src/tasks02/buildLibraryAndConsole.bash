#!/bin/bash

clear

# build library app with .NET Core 2.x
cd Library
dotnet restore
dotnet build -c Release
cd ../

# build console app with .NET Core 2.x
cd Console
dotnet restore
dotnet build -c Release
cd ../
