namespace Library

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

module InternationalStandard =
  
  module BookNumber =
    
    (* https://en.wikipedia.org/wiki/International_Standard_Book_Number *)
    
    type ISBN10 = private ISBN10 of uint64 with
      override isbn10.ToString() =
        let (ISBN10 number) = isbn10
        sprintf "%i" number
    
    type ISBN13 = private ISBN13 of uint16 * uint64 with
      override isbn13.ToString() = 
        let (ISBN13 (prefix,number)) = isbn13
        sprintf "%i-%i" prefix number
    
    let isbn10 : uint64 -> ISBN10 option =
      fun number ->
        (* TODO: add None branch *)
        number |> ISBN10 |> Some
    
    let isbn13 : uint16 -> uint64 -> ISBN13 option =
      fun prefix number ->
        (* TODO: add None branch *)
        (prefix,number) |> ISBN13 |> Some
    
    
module Book =
  
  open InternationalStandard
  
  module Format =
    
    type audio      = AAC  | MP3  | M4B | WAV
    type electronic = EPUB | MOBI | PDF
    type physical   = Hardcover   | Paperback
  
  type author   = Author of name:string * surname:string
  type language = English | Danish | Spanish
  
  type common =
    { title:      string
      authors:    author list 
      publisher:  string
      language:   language
      isbn10:     BookNumber.ISBN10
      isbn13:     BookNumber.ISBN13
    }
  and book =
    | Audio      of audio
    | Electronic of electronic
    | Physical   of physical
  and audio =
    { common: common
      format: Format.audio
    }
  and electronic =
    { common: common
      pages:  uint32
      format: Format.electronic
    }
  and physical =
    { common: common
      pages:  uint32
      format: Format.physical
    }
