﻿// Learn more about F# at http://fsharp.org

open System

open Library.InternationalStandard
open Library.Book
open Library.Book.Format

[<EntryPoint>]
let main argv =
  let isbn10 = BookNumber.isbn10       8420412147UL
  let isbn13 = BookNumber.isbn13 978us 8420412146UL
  
  match isbn10, isbn13 with
    | Some i10, Some i13 ->
      let book =
        { physical.common =
          { title     = "Don Quijote de la Mancha"
            authors   = [ Author ("Miguel", "de Cervantes") ]
            publisher = "Real Academia Española"
            language  = Spanish
            isbn10    = i10
            isbn13    = i13
          }
        ; physical.pages  = 1380u
        ; physical.format = Hardcover
        }
  
      printfn "Book: %A" book
    | __________________ ->
      printfn "No book due to inconrrect ISBN10: %A or ISBN13: %A" isbn10 isbn13
  
  0 // return an integer exit code
