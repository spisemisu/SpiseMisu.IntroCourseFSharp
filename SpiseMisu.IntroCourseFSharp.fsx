#!/usr/bin/env fsharpi

(* This construct is for ML compatibility. The syntax '(typ,...,typ) ident'
   is not used in F# code. Consider using 'ident<typ,...,typ>' instead. *)
#nowarn "62"

(** A few basic concepts *)

module BasicConcepts =

  module FunFirst =

    let foo = [0 .. 9] |> List.map (fun x -> x + x)

    let bar = fun (x,y) -> x + y

    let baz = fun x -> fun y -> x + y

    // let qux = 42 in qux <- 42 (* FS0027: This value is not mutable *)

  module ADT =

    type Foo = int * char

    let foo = (42, 'c')

    type Bar = { foo: int; bar: char }

    let bar = { foo = 42; bar = 'c' }

    type Person      = Child | Adult
    type Temperature = C of float | F of int

    type Baz = { baz: int; qux: char }
    type Qux = Qux of baz:int * qux:char

    type ProductType1 = int * char
    type ProductType2 = { foo: int; bar: char }
    type SumType      = Foo of int | Bar of char

    type FooBar = { foo: int; bar: float }
    let isFoo42 : FooBar -> bool = function
      | { foo = 42 } -> true
      | ____________ -> false

    let assertAge : int -> Person -> bool =
      fun age ->
        function
          | Child -> age <  18
          | Adult -> age >= 18


(** Type/Domain Driven Development *)

module TDDD =

  open System

  type Booking =
    | Basic    of Plane
    | Combo    of Combo
    | FullPack of Plane * Hotel * Car
  and Plane = { Outbound: DateTime; Return:    DateTime; Destination: City }
  and Combo =
    | ``With Hotel`` of Plane * Hotel
    | ``With Car``   of Plane * Car
  and Hotel = { Arrival:  DateTime; Departure: DateTime; Location:    City }
  and Car   = { From:     DateTime; To:        DateTime; Location:    City }
  and City  = String


(** .NET-applications and libraries *)

module netAppsLibs =

  module Lib =
    let someLogic () = 42

  let logic = Lib.someLogic ()

  (* [<EntryPoint>] *)
  let main : string array -> int = fun args -> (* do *) 0

  let foo = fsi.CommandLineArgs

  module FooBar =
    type Foo = private Bar of int
    let  foo = Bar
  
  (* FooBar.Bar 42 *)
  (* FS1093: The union cases … not accessible from this code location *)
  let bar = FooBar.foo 42 (* > val it : FooBar.Foo = FSI_0007+FooBar+Foo *)


(** Data and TypeProviders *)

module DataTypeProviders =

  module DS =

    type tree<'a> =
      | Leaf | Branch of tree<'a> * 'a * tree<'a>
    type lazylist<'a> =
      | Nil  | Cons of 'a * lazylist<unit -> 'a>

  module ML =

    type 'a tree =
      | Leaf | Branch of 'a tree * 'a * 'a tree
    type 'a lazylist =
      | Nil  | Cons of 'a * (unit -> 'a lazylist)


(** Concurrency and parallelism *)

module ConcurrencyParallelism =

  let foo =
    [| 0 .. 10 .. (1 <<< 16) |]
    |> Array.map (fun x -> x * x)

  let bar =
    [| 0 .. 10 .. (1 <<< 16) |]
    |> Array.Parallel.map (fun x -> x * x)

  let asyncHttp : 'a -> 'a Async =
    fun x ->
      (* TODO: Use Don Symes implementation *)
      async { return x }

  let baz =
    [| "https://duckduckgo.com/"; "https://google.com"; "https://bing.com" |]
    |> Array.Parallel.map asyncHttp (* Create async load, no I/O involved  *)
    |> Async.Parallel               (* Retrieve sites concurrently         *)
    |> Async.RunSynchronously       (* Wait for all processes to terminate *)


(** Robust and error-free applications *)

(* Code flows with Maybe monad *)
(fun () ->

 let (>>=) m f = Option.bind f m (* define operator for easy writing           *)

 let inc     = (+) 1                         >> Some
 let dob     = (*) 2                         >> Some
 let div d n = if d = 0 then None else n / d |> Some (* Avoid div by zero      *)

 let foo = 42 |> inc >>= div 0 >>= dob (* None                                 *)
 let bar = 42 |> inc >>= div 1 >>= dob (* Some 86                              *)

 foo,bar
)()

(* Code flows with Either monad*)
(fun () ->
 let bind    f = function | Choice1Of2 r -> f r | Choice2Of2 l -> Choice2Of2 l
 let (>>=) m f = bind f m (* define operator for easy writing                  *)

 let inc     = (+) 1                                             >> Choice1Of2
 let dob     = (*) 2                                             >> Choice1Of2
 let div d n = if d = 0 then Choice2Of2 "Div by zero" else n / d |> Choice1Of2

 let foo = 42 |> inc >>= div 0 >>= dob (* Choice2Of2 "Div by zero"             *)
 let bar = 42 |> inc >>= div 1 >>= dob (* Choice1Of2 86                        *)

 foo,bar
)()
