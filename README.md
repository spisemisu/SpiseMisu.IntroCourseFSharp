Intro Course in F#
==================

Come to an Introductory Course in F# - A mature, open source, multi-platform,
functional .NET programming language that allows users and organizations to cope
with complex issues with simple, robust and maintenance-friendly applications.

### Target group and prerequisites:

The course is aimed form people that want to get a good start when developing
.NET libraries and applications with F#. Participants must have a minimum
knowledge of other programming languages. Knowledge of other functional
programming languages will be an advantage, but not crucial.

### Program:

* A few basic concepts to get started
* Domain modeling
* .NET-applications and libraries
* Data and TypeProviders
* Concurrency and parallelism
* Robust and error-free applications

### Teacher:

Ramón Soto Mathiesen is a computer scientist and independent IT consultant at
SPISE MISU ApS, as well as one of the driving forces behind Functional
Copenhageners Meetup Group.

### Comments:

#### First:

Since we only have 4 hours, we expect that you have installed F# in advance on
your laptop. Here are guides to relatively the most common operating systems:

* Linux: http://fsharp.org/use/linux/
* Mac: http://fsharp.org/use/mac/
* Windows: http://fsharp.org/use/windows/

The guides should be easy to follow. If you have any questions, please don't
hesitate to ask them here.

#### Second:

As we can't assume that everybody will have the same operating system, please
ensure that you also have installed .NET Core 2.X as described here:

* https://www.microsoft.com/net/core
